const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server, {
  cors: {
      origin: "*",
      methods: ["GET", "POST"]
  }
});
const PORT = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.send('<h1>Hola.</h1>');
});

app.get('/hayhamburguesa', (req, res) => {
  if(req.query.id){
    console.log('Enviando señal hayHamburguesa');
    io.to(req.query.id).emit('hayHamburguesa');
    res.send('<h1>Señal enviada a pantalla ' + req.query.id + ': Hay hamburguesa!</h1>');
  } else {
    res.send('<h1>Se requiere ID</h1>');
  }
});

app.get('/robarhamburguesa', (req, res) => {
  if(req.query.id){
    console.log('Enviando señal robarHamburguesa');
    io.to(req.query.id).emit('robarHamburguesa');
    res.send('<h1>Señal enviada a pantalla ' + req.query.id + ': Robar hamburguesa!</h1>');
  } else {
    res.send('<h1>Se requiere ID</h1>');
  }
});

// TODO: Hacer endpoint para consultar último estado de hamburguesas en una pantalla.

io.on('connection', (socket) => {
  console.log('Pantalla conectada');
  socket.on('joinRoom', function(room) {
    console.log('Uniendo a room: ', room);
    socket.join(room);
  });
});

server.listen(PORT, () => {
  console.log('Listening on port: ', PORT);
});